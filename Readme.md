# VirusTotal URL Scanner - Stay protected.
browser extension

<p align="center">
  <a href="https://addons.mozilla.org/en-US/firefox/addon/scan-website" rel="nofollow">
    <img src="https://i.imgur.com/kMH6r1a.png" style="max-width:100%;"></a>

  <a href="https://microsoftedge.microsoft.com/addons/detail/pkejjdammeaholmlamljeandiahhnddf" rel="nofollow">
    <img src="https://i.imgur.com/n49Wiu2.png" style="max-width:100%;"></a>
  <br><br>
</p>

# Captures
![capture.png](capture.png)

![capture1.png](capture1.png)

# License
MIT