export type ScanResult = {
  type: string
  id: string
}

export type URLAnalysisReport = {
  last_modification_date: number
  times_submitted: number
  total_votes: {
    harmless: number
    malicious: number
  },
  threat_names: string[]
  redirection_chain: string[]
  last_submission_date: number
  last_http_response_content_length: number
  last_http_response_headers: object
  reputation: number
  tags: string[]
  last_analysis_date: number
  first_submission_date: number
  categories: object
  last_http_response_content_sha256: string
  last_http_response_code: number
  last_final_url: string
  trackers: object
  url: string
  title: string
  last_analysis_stats: {
    harmless: number,
    malicious: number,
    suspicious: number,
    undetected: number,
    timeout: number
  }
  last_analysis_results: object
  html_meta: object
}