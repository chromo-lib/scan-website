import { ScanResult, URLAnalysisReport } from "../types";

const VIRUS_TOTAL_BASE_URL = "https://www.virustotal.com/api/v3/urls";

export default class VirusTotalV3 {

  static async getScanID(url: string): Promise<string | null> {
    const API_KEY = await this.getKey();

    if (!API_KEY) {
      await chrome.runtime.sendMessage({ error: 'no key' });
      return null;
    }

    const headers = { 'x-apikey': API_KEY };
    const body = new FormData();
    body.append('url', url);

    const response = await fetch(VIRUS_TOTAL_BASE_URL, { method: 'POST', body, headers });
    const { data } = await response.json();
    return (data as ScanResult).id.split('-')[1];
  }

  static async getURLAnalysisReport(url: string): Promise<URLAnalysisReport | undefined> {
    try {
      const id = await this.getScanID(url);

      const API_KEY = await this.getKey();
      const headers = { 'x-apikey': API_KEY };

      const response = await fetch(VIRUS_TOTAL_BASE_URL + '/' + id, { headers });
      const json = await response.json();

      return json.data.attributes;
    } catch (error) {
      return undefined
    }
  }

  static async getKey() {
    const storage = await chrome.storage.local.get('VIRUS_TOTAL_API_KEY');
    return storage && storage.VIRUS_TOTAL_API_KEY ? storage.VIRUS_TOTAL_API_KEY : null;
  }

  static async setKey(key: string) {
    const res = await chrome.storage.local.set({ ['VIRUS_TOTAL_API_KEY']: key });
  }
}