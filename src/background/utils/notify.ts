export default function notify(options: any) {
  return new Promise<void>((resolve, reject) => {
    chrome.notifications.create('' + Date.now(), {
      iconUrl: chrome.runtime.getURL("icons/128.png"),
      title: 'VirusTotal Scan result',
      type: 'basic',
      ...options
    }, () => { resolve(); });
  });
}