export default function createMenuContext() {
  function onCreated() {
    if (chrome.runtime.lastError) {
      const err = chrome.runtime.lastError;
      console.log(`Error: ${typeof err === 'string' ? err : JSON.stringify(err)}`);
    } else {
      console.log("contextMenus created successfully");
    }
  }

  chrome.contextMenus.create(
    {
      id: "scan-website",
      title: "Scan This Website",
      contexts: ['all'],
    },
    onCreated
  );
}