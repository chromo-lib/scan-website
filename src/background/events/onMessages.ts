import VirusTotalV3 from "../utils/VirusTotalV3";

export default function onMessages(request: any, _: any, sendResponse: any) {
  try {
    if (request.message === 'save:key' && request.key) {
      VirusTotalV3.setKey(request.key);
      sendResponse({ apikey: request.key });
    }

    if (request.message === 'get:key') {
      sendResponse('get:key');
      VirusTotalV3.getKey().then(apikey => { chrome.runtime.sendMessage({ apikey }); });
    }

    if (request.message === 'scan:url' && request.url) {
      sendResponse('scan:url');
      VirusTotalV3.getURLAnalysisReport(request.url)
        .then(scanResult => { chrome.runtime.sendMessage({ scanResult }); })
        .catch(e => { chrome.runtime.sendMessage({ error: e.message }); })
    }

    if (request.message === 'clear:data') {
      sendResponse('clear:data');
      chrome.storage.local.clear();
    }
  } catch (error: any) {
    if (!error.message.includes('JSON')) chrome.storage.local.clear();
    chrome.runtime.sendMessage({ error: error.message });
  }
}