import notify from "../utils/notify";
import VirusTotalV3 from "../utils/VirusTotalV3";

export default async function onContextMenu(info: chrome.contextMenus.OnClickData, tab?: chrome.tabs.Tab | undefined) {
  try {
    if (!tab || !tab.url) return;

    const result = await VirusTotalV3.getURLAnalysisReport(tab.url);

    if (result && result.last_analysis_stats) {
      const message = `${result.last_analysis_stats.harmless > 0 ? 'Safe' : 'Is not safe'}
URL: ${new URL(result.url).origin}
`;

      notify({ message });
    }
  } catch (error: any) {
    notify({ message: error.message });
  }
}