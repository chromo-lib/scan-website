import onContextMenu from "./events/onContextMenu";
import onMessages from "./events/onMessages";
import createMenuContext from "./utils/createMenuContext";

createMenuContext();

chrome.contextMenus.onClicked.addListener(onContextMenu);
chrome.runtime.onMessage.addListener(onMessages);
