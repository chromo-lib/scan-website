import { URLAnalysisReport } from "../types";

export default class VirusTotal {
  static getDetected(last_analysis_results: any) {
    return last_analysis_results
      ? Object.keys(last_analysis_results)
        .filter((key: any) => {
          const result = last_analysis_results[key].result;
          return result === 'malware' || result === 'malicious' || result === 'suspicious'
        })
      : [];
  }

  static getObjectLen(last_analysis_stats: object) {
    return Object.keys(last_analysis_stats).length
  }

  static saveScanResult(scanResult: object) {
    localStorage.setItem('scanResult', JSON.stringify(scanResult));
  }

  static getScanResult(): URLAnalysisReport | null {
    try {
      const result = localStorage.getItem('scanResult')
        ? JSON.parse(localStorage.getItem('scanResult') as any)
        : {};

      return result && result.last_analysis_date ? result : null;
    } catch (error) {
      localStorage.clear();
      location.reload()
      return null
    }
  }

  static clear() {
    localStorage.removeItem('scanResult');
    localStorage.removeItem('VIRUS_TOTAL_API_KEY');
  }
}