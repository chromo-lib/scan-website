import {h, Fragment} from 'preact'
import './Spinner.css';

export default function Spinner() {
  return <div className="w-100 h-100 spinner-container text-white text-uppercase">
    <div className="spinner-border mb-5" role="status"></div>
    <p className="ltsp2">Please be patient until the scan is completed</p>
  </div>;
}