import { h, Fragment } from 'preact'
import { useContext, useMemo, useState } from "preact/hooks";
import IconCircle from "../icons/IconCircle";
import IconCrossBones from "../icons/IconCrossBones"
import IconGlobe from '../icons/IconGlobe';
import IconInfo from '../icons/IconInfo';
import IconUserShield from "../icons/IconUserShield"
import VirusTotal from '../services/VirusTotal';
import { GlobalContext } from '../State';
import { IContext, URLAnalysisReport } from '../types';

const removeDuplicates = (arr: string[] | undefined) => {
  if (!arr) return undefined;
  const result: string[] = [];
  arr.forEach(d => {
    if (!result.includes(d)) result.push(d);
  });
  return result
}

const ListDetected = ({ detected }: any) => {
  if (!detected || detected.length < 1) return <Fragment></Fragment>

  return <div class="w-100 box bg-dark mb-1 overflow">
    <p class="gray mt-0">{detected.length} security vendors flagged this domain as malicious</p>
    <ul class="w-100 grid-2 justify-between p-0">
      {detected.map((name: string, i: number) => <li class="d-flex align-center" key={i}>
        <IconCircle />
        <span class="ml-1 truncate" title={name}>{name}</span>
      </li>)}
    </ul>
  </div>
}

export default function ScanResult() {
  const { state }: IContext = useContext<IContext>(GlobalContext);

  const [showMore, setShowMore] = useState(false);

  if (!state || !state.scanResult) return <div class="bordered br7 mb-1">
    <img class="w-100" src="/www-bro.svg" alt="website scan" />
  </div>

  const scanResult: URLAnalysisReport = state.scanResult;

  const detected = useMemo(() => VirusTotal.getDetected(scanResult.last_analysis_results), [scanResult]);

  const onShowMore = () => {
    setShowMore(!showMore)
  }

  return <Fragment>
    <div class='w-100 box bg-dark mb-1 bordered'>
      <h3 class='d-flex align-center m-0' title={scanResult.url}>
        <IconGlobe clx='mr-1' />
        {new URL(scanResult.url).hostname}
      </h3>
      <small title="Website title" class="gray">{scanResult?.title}</small>
      <div class="d-flex flex-wrap mt-1 wb">
        {removeDuplicates(Object.values(scanResult.categories))?.map((value, i) => <span class="tag br7" title="category" key={i}>{value}</span>)}
      </div>
    </div>

    <div class='w-100 box grid-2-1 justify-between align-center bg-dark mb-1 bordered'>
      <div>
        <h1 className={scanResult.last_analysis_stats.malicious > 0 ? 'm-0 red' : 'm-0 green'}>
          {scanResult.last_analysis_stats.malicious > 0 ? 'Is not Safe' : 'Safe'}
        </h1>

        <div class="d-flex flex-wrap gray mb-1 wb">
          {removeDuplicates(scanResult.redirection_chain)?.map((value, i) => <small class="mr-1" title="redirection_chain" key={i}>{value}</small>)}
        </div>

        <div class="w-100 mt-1">
          <small class="white">Scan detection: {detected.length} / {VirusTotal.getObjectLen(scanResult.last_analysis_results)}</small>
          <progress class="w-100 mt-1" min={0} value={detected.length} max={VirusTotal.getObjectLen(scanResult.last_analysis_results)}></progress>
        </div>
      </div>

      <div>
        {scanResult.last_analysis_stats.malicious > 0 ? <IconCrossBones /> : <IconUserShield />}
        <div class="d-flex gray mt-1">
          <small class="mr-1" title="Total community votes: harmless">H {scanResult.total_votes.harmless}</small>
          <small class="mr-1">|</small>
          <small title="Total community votes: malicious">M {scanResult.total_votes.malicious}</small>
        </div>
      </div>
    </div>

    <ListDetected detected={detected} />

    {showMore && <pre class="m-0 bordered br7 mb-1">{JSON.stringify(scanResult, null, 2)}</pre>}

    <button class="w-100" onClick={onShowMore}><IconInfo clx='mr-1' />{showMore ? 'Less' : 'More'} Infos</button>

    <hr />
  </Fragment>
}
