import { h } from 'preact';
import { useCallback } from "preact/hooks";
import IconTrash from "../icons/IconTrash";
import VirusTotal from '../services/VirusTotal';

export default function ClearData() {
  const onClearData = useCallback(() => {
    if (confirm('Confirm clear all data in local storage?')) {
      chrome.runtime.sendMessage({ message: 'clear:data' }, response=>{
        VirusTotal.clear();
        window.location.reload();
      });
    }
  }, []);

  return <button class="w-100" onClick={onClearData}><IconTrash clx="mr-1" />clear data</button>
}