import { h, Fragment } from 'preact'
import { useContext, useEffect } from 'preact/hooks';
import FormScan from './forms/FormScan';
import FormSaveKey from './forms/FormSaveKey';
import ScanResult from './components/ScanResult';
import ClearData from './components/ClearData';
import VirusTotal from './services/VirusTotal';
import { GlobalContext } from './State';
import Spinner from './components/Spinner';
import { IContext } from './types';

export default function App() {
  const { state, setState }: IContext = useContext<IContext>(GlobalContext);

  const onMessages = (request: any, _: any, sendResponse: any) => {
    sendResponse('');

    if (chrome.runtime.lastError) return;

    if (request.error) { 
      const error = typeof request.error === 'string' ? request.error : JSON.stringify(request.error);
      setState({ ...state, message: error, isScanning: false });
     }
    
    if (request.apikey) {
      localStorage.setItem('apikey', 'apikey');
      setState({ ...state, apikey: request.apikey });
    }

    if (request.scanResult && request.scanResult.last_modification_date) {
      setState({ ...state, scanResult: request.scanResult, isScanning: false, apikey: localStorage.getItem('apikey') });
      VirusTotal.saveScanResult(request.scanResult);
    }
  }

  useEffect(() => {
    chrome.runtime.onMessage.addListener(onMessages);
    chrome.runtime.sendMessage({ message: 'get:key' }, response => {
      if (chrome.runtime.lastError) localStorage.clear();
      else setState({ ...state, apikey: response.apikey });
    });

    return () => {
      chrome.runtime.onMessage.removeListener(onMessages);
    }
  }, []);

  return <Fragment>
    {state.apikey ? <FormScan /> : <FormSaveKey />}

    <hr />

    {state.isScanning
      ? <Spinner />
      : <ScanResult />}

    {state.message && <pre class="bg-red m-0 mb-1 br7">{state.message}</pre>}

    {state.apikey && <ClearData />}

    <hr />

    <footer class="w-100 d-flex justify-between">
      <a class="dark" href="https://www.virustotal.com/gui/home/url"
        target="_blank" rel="noreferrer noopener"
        title="Virustotal website">Virustotal website</a>

      <a class="dark" href="https://gitlab.com/chromo-lib/scan-website"
        target="_blank" rel="noreferrer noopener"
        title="Project Repository">Repository</a>
    </footer>
  </Fragment>
}