import { h } from 'preact'
import { useContext } from "preact/hooks";
import IconSearch from "../icons/IconSearch";
import { GlobalContext } from '../State';
import { useThrottledCallback } from 'use-debounce';

export default function FormScan() {
  const { state, setState }: any = useContext(GlobalContext);

  const onScanURL = useThrottledCallback(async (e: any) => {
    e.preventDefault();
    setState({ ...state, isScanning: true });
    try {
      const url: string = e.target.elements[0].value.trim().toLowerCase();
      await chrome.runtime.sendMessage({ message: 'scan:url', url });
      e.target.reset();
    } catch (error: any) {
      setState({ ...state, isScanning: false, message: error.message });
    }
  }, 1000);

  return <form class="w-100 d-flex" onSubmit={onScanURL}>
    <input class="w-100" type="search" name="url" id="url" placeholder="google.com" required />
    <button type="submit" title="Start Scan" disabled={state.isScanning}><IconSearch /></button>
  </form>
}
