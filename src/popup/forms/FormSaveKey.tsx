import { h, Fragment } from 'preact'
import { useCallback, useContext } from "preact/hooks";
import IconKey from "../icons/IconKey";
import { GlobalContext } from '../State';
import { IContext } from '../types';

export default function FormSaveKey() {
  const { state, setState }: IContext = useContext<IContext>(GlobalContext);

  if (state.apikey) return <Fragment></Fragment>

  const onSaveKey = useCallback((e: any) => {
    e.preventDefault();
    const key = e.target.elements[0].value.trim();
    chrome.runtime.sendMessage({ message: 'save:key', key }, response => {
      if (!chrome.runtime.lastError && response) setState({ ...state, apikey: response.apikey })
    });
  }, []);

  return <form onSubmit={onSaveKey}>
    <label htmlFor="VIRUS_TOTAL_API_KEY"><IconKey clx="mr-1" />VIRUSTOTAL API KEY</label>
    <input class="w-100 mb-1 mt-1" type="password" name="VIRUS_TOTAL_API_KEY" id="VIRUS_TOTAL_API_KEY" placeholder="xxxxxxx" required />
    <a class="dark" href="https://www.virustotal.com/gui/user/bestprod/apikey" target="_blank">Get VIRUSTOTAL API KEY (it's free)</a>
    <button class='w-100 mt-1' type="submit">Save key</button>
  </form>
}
