import { createContext } from 'preact';
import { useState } from 'preact/hooks';
import VirusTotal from './services/VirusTotal';
import { IState } from './types';

const initState: IState = {
  scanResult: VirusTotal.getScanResult(),
  message: null,
  isScanning: false,
  apikey: localStorage.getItem('apikey')
}

export const GlobalContext: any = createContext<any>(null);

export function GlobalProvider({ children }: any) {

  const [state, setState] = useState(initState);

  return (
    <GlobalContext.Provider value={{ state, setState }}>
      {children}
    </GlobalContext.Provider>
  )
}
