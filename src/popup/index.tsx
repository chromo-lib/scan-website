import { h, render } from 'preact'
import App from './app'
import { GlobalProvider } from './State';
import './index.css';

render(
  <GlobalProvider>
    <App />
  </GlobalProvider>,
  document.getElementById('app') as HTMLElement
)